import React, { createContext } from "react";
import { AppReducer } from "../reducers/AppReducer";
import { 
    FETCH_DATA,
    FETCH_NEXT,
    FETCH_PREV,
    FETCH_RESET,
    CAPTURE, 
    RELEASE, 
    ADD_POKEMON, 
    ADD_POKEMONS, 
    UPDATE_POKEMON 
} from '../constants/AppActions';

const AppContext = createContext();
const AppProvider = ({ children }) => {
    const [state, dispatch] = AppReducer();
    const { pokemons, capturedPokemons, offset, limit } = state;

    const capture = (pokemon) => () => dispatch({ type: CAPTURE, pokemon });
    const release = (pokemon) => () => dispatch({ type: RELEASE, pokemon });
    const addPokemon = (pokemon) => dispatch({ type: ADD_POKEMON, pokemon });
    const addPokemons = (pokemons) => dispatch({ type: ADD_POKEMONS, pokemons });
    const updatePokemon = (pokemon) => dispatch({ type: UPDATE_POKEMON, pokemon });
    const fetchData = (pokemons) => dispatch({ type: FETCH_DATA, pokemons });
    const fetchNext = () => dispatch({ type: FETCH_NEXT });
    const fetchPrev = () => dispatch({ type: FETCH_PREV });
    const fetchReset = () => dispatch({ type: FETCH_RESET });

    const providerValue = {
        pokemons,
        capturedPokemons,
        offset,
        limit,
        capture,
        release,
        addPokemon,
        addPokemons,
        updatePokemon,
        fetchData,
        fetchNext,
        fetchPrev,
        fetchReset,
  };

  return (
    <AppContext.Provider value={providerValue}>
      {children}
    </AppContext.Provider>
  )
};

export { AppContext, AppProvider };