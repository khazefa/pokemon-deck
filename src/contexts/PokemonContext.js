import { createContext, useMemo } from 'react';
import { PokemonReducer } from '../reducers/PokemonReducer';
import { CAPTURE, RELEASE } from '../constants/AppActions';


const PokemonContext = createContext();

const PokemonProvider = (props) => {
  const [state, dispatch] = PokemonReducer();
  const { pokemons, capturedPokemons } = state;

  const capture = (pokemon) => () => dispatch({ type: CAPTURE, pokemon });
  const release = (pokemon) => () => dispatch({ type: RELEASE, pokemon });

  const providerValue = {
    pokemons,
    capturedPokemons,
    capture,
    release
  };

  const ProviderValue = useMemo(() => ({
    // useMemo calls the passed function only when necessary and returns cached values
    pokemons, capturedPokemons, capture, release
  }),
    [pokemons, capturedPokemons, capture, release]
  );

  return (
    <PokemonContext.Provider value={ProviderValue}>
      {props.children}
    </PokemonContext.Provider>
  )
};

export { PokemonContext, PokemonProvider };