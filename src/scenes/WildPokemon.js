import React, { useState, useContext, useEffect, useCallback } from 'react';
import { Helmet } from 'react-helmet';
import {
    Grid,
    Header,
    Button,
} from 'semantic-ui-react';
import { useLocation } from 'react-router-dom';

import apiConnect from '../utils/HttpCommons';
import { AppContext } from '../contexts/AppContext';
// import { GET_POKEMONS, GET_POKEMONS_VARS } from '../graphql/queries';
import PokemonCard from '../components/card/PokemonCard';


const WildPokemon = () => {
    const { 
        pokemons, capturedPokemons, offset, limit, addPokemons, fetchNext 
    } = useContext(AppContext);

    let location = useLocation();

    const showOwnedCount = ({ location }) => {
        return location.pathname !== '/' ? true : false;
    }

    const isOwnedPokemon = () => {
        return true;
    }

    // const [pokemonx, setPokemons] = useState([
    //     { id: 1, name: 'Bulbasaur' },
    //     { id: 2, name: 'Pikachu' },
    //     { id: 3, name: 'Duoduo' },
    //     { id: 4, name: 'Ivysaur' },
    //     { id: 5, name: 'Charmeleon' },
    //     { id: 6, name: 'Wartortle' },
    //     { id: 7, name: 'Butterfree' },
    // ]);

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');
    const [ownedPokemon, setOwnedPokemon] = useState(0);

    const ownedCount = useCallback( (pokemon) => {
        return capturedPokemons.filter((myPokemon) => {
            return myPokemon.real_id === pokemon.id;
        }).length;
    },[capturedPokemons]);

    const getPokemonData = useCallback(async (pokemon) => {
        try {
            const response = await apiConnect.get(`/pokemon/${pokemon.name}`);

            let _pokemon_data = response.data;
            let {id, name, sprites} = _pokemon_data;
            
            return {
                id,
                name,
                sprites,
                owned_count: ownedCount(_pokemon_data)
            };

        } catch (error) {
            console.log('error getting pokemon data :(', error);

            return {
                ...pokemon
            };
        }
    },[ownedCount]);

    useEffect(() => {
        console.log('limit:' + limit);
        console.log('pokemons:' + JSON.stringify(pokemons));
        const fetchData = async () => {
            const response  = await apiConnect.get('/pokemon', {
                params: {
                    offset: offset,
                    limit: limit,
                }
            });
            await Promise.all(response.data.results.map((pokemon) =>
                {
                    return getPokemonData(pokemon);
                })
            )
            .then((pokemons) => {
                let payload = {
                    pokemons
                }
                addPokemons(payload);
            })
            .catch((error) => {
                console.log('Error fetching detail data: ' + error.message);
            }) 
        };
        fetchData();
    }, []);

    return (
    <>
        <Helmet>
            <title>{"Pokemon List - " + process.env.REACT_APP_NAME}</title>
            <meta name="description" content={"Pokemon List - " + process.env.REACT_APP_NAME} />
        </Helmet>
        <Header as="h2" color="orange" textAlign="center">Pokemon List</Header>
        {/* <Grid columns={4} stackable> */}
        { loading && 'Loading...' }
        { error && `Error! ${error.message}` }
        { console.log('Response from server', JSON.stringify(pokemons)) }
        <Grid columns={4} doubling>
        {
            (pokemons.pokemons) ? 
                pokemons.pokemons.map((pokemon) => {
                    return (
                        <PokemonCard key={pokemon.id} index={pokemon.id} name={pokemon.name} sprites={pokemon.sprites} showOwnedCount='true' ownedCount={ownedPokemon} detailUrl={`/pokemon-detail/${pokemon.id}`} />
                    )
                }) : ('')
        }
        </Grid>
        <Grid columns={1} doubling>
            <Grid.Column>
                <Button color='blue' onClick={fetchNext()}>
                    Catch Pokemon
                </Button>
            </Grid.Column>
        </Grid>
    </>
    );
};

export default React.memo(WildPokemon);