import React from 'react';
import { Helmet } from 'react-helmet';
import { withRouter } from "react-router-dom";

import { AppProvider } from '../contexts/AppContext';
import Routes from '../routes/index';
import TopBar from '../components/layout/TopBar';
import FooterMenu from '../components/layout/FooterMenu';

const Home = () => (
    <>
        <AppProvider>
            <Helmet>
                <title>{process.env.REACT_APP_NAME}</title>
                <meta name="description" content={process.env.REACT_APP_NAME} />
            </Helmet>
            <TopBar />
            <Routes />
            <FooterMenu />
        </AppProvider>
    </>
)

export default withRouter(Home);