import { React, Suspense } from 'react';
import {
    Switch,
    Route,
} from 'react-router-dom';
import { 
    Container,
    Dimmer,
    Loader,
} from 'semantic-ui-react';

import routes from './AppRoutes';

const index = _ => {

    return (
        <div>
        <Container style={{ marginTop: '6em', paddingBottom: '6em' }}>
            <Switch>
                <Suspense fallback={
                    <Dimmer active>
                        <Loader content='Loading' />
                    </Dimmer>
                }>
                    {
                        routes.map(({path, Component}, routeIndex) => {
                            return <Route 
                                key={routeIndex} 
                                exact 
                                path={path}
                                component={Component}
                                />
                        })
                    }
                </Suspense>
            </Switch>
        </Container>
        </div>
    );
};

export default index;