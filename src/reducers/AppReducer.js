import { useReducer } from 'react';
import { 
    FETCH_DATA,
    FETCH_NEXT,
    FETCH_PREV,
    FETCH_RESET,
    CAPTURE, 
    RELEASE, 
    ADD_POKEMON, 
    ADD_POKEMONS, 
    UPDATE_POKEMON 
} from '../constants/AppActions';

const getPokemonsList = (pokemons, capturedPokemon) =>
  pokemons.filter(pokemon => pokemon !== capturedPokemon)

const capturePokemon = (pokemon, state) => ({
  pokemons: getPokemonsList(state.pokemons, pokemon),
  capturedPokemons: [...state.capturedPokemons, pokemon]
});

const getCapturedPokemons = (capturedPokemons, releasedPokemon) =>
  capturedPokemons.filter(pokemon => pokemon !== releasedPokemon)

const releasePokemon = (releasedPokemon, state) => ({
  pokemons: [...state.pokemons, releasedPokemon],
  capturedPokemons: getCapturedPokemons(state.capturedPokemons, releasedPokemon)
});

const addPokemon = (pokemon, state) => ({
  pokemons: [...state.pokemons, pokemon],
  capturedPokemons: state.capturedPokemons
});

const addPokemons = (pokemons, state) => ({
  pokemons: pokemons,
  capturedPokemons: state.capturedPokemons
});

const updatePokemon = (capturedPokemon, state) => ({
    capturedPokemons: [...state.capturePokemons].map((pokemon) => {
        if (pokemon.id === capturedPokemon.id) {
            return {
                ...capturedPokemon
            }
        }
        return capturedPokemon;
    })
});

const countCapturedPokemons = (pokemons, capturedPokemon) => {
    return pokemons.filter((pokemon) => {
        return pokemon.real_id === capturePokemon.id;
    }).length;
}


const pokemonReducer = (state, action) => {
    // reducer is going to update part of the app state
    // action.type is defining what is to be updated
    // reducer returns the new state

    //  action object
    // {
    // type: FETCH_DATA
    // pokemons: someValue
    // }

    switch (action.type) {
        case FETCH_DATA:
            return {
                // we want to keep a copy of the existing state
                ...state.pokemons,
                // the elements in the state we want to change
                pokemons: action.pokemon,
            };
        case FETCH_NEXT:
            return {
                ...state.pokemons,
                limit: state.offset + state.limit,
                offset: 0,
            };
        case FETCH_PREV:
            return {
                // ...state.pokemons,
                limit: state.offset - state.limit,
                offset: 0,
            };
        case FETCH_RESET:
            return {
                // ...state.pokemons,
                limit: 0,
                offset: 0,
            };
        case CAPTURE:
            return capturePokemon(action.pokemon, state);
        case RELEASE:
            return releasePokemon(action.pokemon, state);
        case UPDATE_POKEMON:
            return updatePokemon(action.pokemon, state);
        case ADD_POKEMON:
            return addPokemon(action.pokemon, state);
        case ADD_POKEMONS:
            return addPokemons(action.pokemons, state);
        default:
            return state;
    }
};

// export default AppReducer;
export const AppReducer = () =>
  useReducer(pokemonReducer, {
    pokemons: [],
    capturedPokemons: [],
    offset: 0,
    limit: 20,
  });